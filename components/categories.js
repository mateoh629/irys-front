import React from 'react'
import { useSelector } from 'react-redux'
import { View , Text, StyleSheet, Image, FlatList, TouchableOpacity } from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import { useTranslation } from 'react-i18next'

export function Categories( { navigation , ...props } ) {

  const list = useSelector(state => state.categoriesReducer.categories)
  const language = useSelector(state => state.languageReducer.lang)
  const{ t } = useTranslation()

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{t('categories.title')}</Text>
      <Text style={styles.subTitle}>{t('categories.subTitle')}</Text>
      <FlatList
        data={list.sort((a, b) => a.order - b.order)}
        renderItem={({item}) => (
          <View key={item.order}>
            <TouchableOpacity
              onPress={() => {
                item.requireDescription ? navigation.navigate('subcategories') : alert(t('categories.alert'))
              }}
              style={styles.cat}
            >
              <View style={styles.catText}>
                <Image 
                    source = {{ uri : item.icon}}
                    style = {{ height: 40, width: 40, borderRadius: 1 }}
                />
                <Text style={styles.category}>{language === 'en' ? item.name.es : item.name.en}</Text>
              </View>
              <Ionicons name='chevron-forward-outline' size={15}/>
            </TouchableOpacity>
          </View>
        )}
        style={styles.list}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffe8e8',
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingTop: 50
  },
  title: {
    fontWeight: 'bold',
    fontSize: 25,
    margin: 10,
    fontFamily: 'Helvetica'
  },
  subTitle: {
    margin: 10,
    fontSize: 20,
    fontFamily: 'Helvetica'
  },
  cat: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: 400,
    margin: 10
  },
  category: {
    borderBottomWidth: 5,
    borderBottomColor: '#000000',
    padding: 20, 
    fontSize: 15,
    fontFamily: 'Helvetica'
  }, 
  catText: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#000000',
    borderBottomWidth: 1,
    width: 400
  }
})