## Run the app

These are the steps you must follow in order to get the app running.

1. Clone this repository.
2. Move to the project's directory, and then to irys-front folder.
3. Run npm i on the console to install required dependencies.
4. On the console, type **npm run ios** and press **Enter**, it should automatically start the iOS emulator. If you want to run the app on Android, type **npm run android** after initializing an Android emulator, but the most important is, you have to change the word **localhost** for your own IP address in **screens/home.js** on line 24, for http requests to work on android, this is not needed if running iOS emulator.
5. **Important:** remember that before you start the app (frontend), you must already have started the server (backend), by following the steps on backend's repository **README**.

---

## About the project

Here I'll explain some things about the code, why and how I wrote the app.

1. Built using React Native (Expo).
2. Language is JavaScript.
3. Axios for http requests.
4. React-redux for global state management, this was not a required feature, but I wanted to show redux usage.
5. Use of function components and hooks.
6. There's a **utils** folder where useful tools are located.
7. In the app you can find several types of navigation: **stack**, **tab** and **drawer** navigation.
8. From any of the screens in Home tab, you can swipe right to open the drawer, in there you can change the app's language, choosing between english and spanish with the button (for the first time, you have to press twice, after that, only one press is needed to change the language) in the drawer.
9. I didn't actually focus on styles, the app was designed for iPhone 12 Pro Max mainly, it will work on android and smaller iPhones, but will look its best on iPhone 12 Pro Max's emulator. I focused on functionalities and on showing different technologies' usage, such as redux, i18n, navigators, .
10. This app is not mocking data from a JSON file, it is in fact fetching data from an actual Firestore database I created, through a server I wrote using NodeJS.
11. In the map, the user will find 2 buttons, a colored one with a legend at the bottom left ('Get current location'), that will emit an alert showing the exact coordinates, and another one at the bottom right (usual location icon) that will navigate and show the user's current location, and pin a marker in there.

---
