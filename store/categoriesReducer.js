const RECEIVE = 'RECEIVE'
const NODATA = 'NODATA'

export function receive(data) {
  return {
    type: RECEIVE,
    payload: data
  }
}

export function nodata() {
  return {
    type: NODATA
  }
}

const initialState = {
    categories: [],
    subcategories: [],
    message: ''
}

export function categoriesReducer(prevState = initialState, action) {
  switch(action.type) {
    case RECEIVE:
      return {
        ...prevState,
        categories: action.payload.map(category => ({
            name: category.name,
            icon: category.icon, 
            order: category.order,
            requireDescription: category.requireDescription
        })),
        subcategories: action.payload.map(category => (
          category.subCategories || null
        ))
      }
    case NODATA:
      return {
        message: 'No data received'
      }
    default:
      return prevState
  }
}