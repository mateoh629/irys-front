import React from 'react'
import { useSelector } from 'react-redux'
import { View , Text, StyleSheet, Image, FlatList, TouchableOpacity } from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import { useTranslation } from 'react-i18next'
import { icon } from '../utils/icon'

export function SubCategories( { navigation, ...props } ) {

  const list = useSelector(state => state.categoriesReducer.subcategories)
  const language = useSelector(state => state.languageReducer.lang)
  const{ t } = useTranslation()

  let subcategoriesList = []
  for (let key in list) {
    if (list[key] !== null) {
      for (let subKey in list[key]) {
        subcategoriesList.push(list[key][subKey])
      }
    }
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{t('subcategories.title')}</Text>
      <Text style={styles.subTitle}>{t('subcategories.subTitle')}</Text>
      <FlatList
        data={subcategoriesList}
        renderItem={({item}) => (
          <View key={item.name.en}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('addDescription')
              }}
              style={styles.cat}
            >
              <View style={styles.catText}>
                <Ionicons name={icon(item.name.en)} size={20}/>
                <Text style={styles.category}>{language === 'en' ? item.name.es : item.name.en}</Text>
              </View>
              <Ionicons name='chevron-forward-outline' size={15}/>
            </TouchableOpacity>
          </View>
        )}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffe8e8',
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingTop: 50
  },
  title: {
    fontWeight: 'bold',
    fontSize: 25,
    margin: 10,
    fontFamily: 'Helvetica',
    textAlign: 'left'
  },
  subTitle: {
    margin: 10,
    fontSize: 20,
    fontFamily: 'Helvetica'
  },
  cat: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: 400,
    margin: 10,
    borderBottomColor: '#000000',
    borderBottomWidth: 1
  },
  category: {
    borderBottomWidth: 5,
    borderBottomColor: '#000000',
    paddingLeft: 15,
    fontSize: 15,
    fontFamily: 'Helvetica'
  },
  catText: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  }
})