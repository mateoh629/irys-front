import React from 'react'
import { StyleSheet } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { Ionicons } from '@expo/vector-icons'
import { Provider } from 'react-redux'
import { store } from './store/store'
import { Home } from './screens/home'
import { Map } from './screens/map'
import './i18n'
import { useTranslation } from 'react-i18next'

const Tab = createBottomTabNavigator()

export default function App() {

  const { t } = useTranslation()

  return (
    <Provider store={store}>
      <NavigationContainer>
        <Tab.Navigator>
          <Tab.Screen 
            name={t('tab.home') }
            component={Home}
            options = {{
              tabBarIcon : ({ color , size }) => (
                <Ionicons name = 'ios-home' size = {size} color = {color} />
              )
            }}
          />
          <Tab.Screen 
            name={t('tab.map')}
            component={Map}
            options = {{
              tabBarIcon : ({ color , size }) => (
                <Ionicons name = 'ios-earth' size = {size} color = {color} />
              )
            }}
          />
        </Tab.Navigator>
      </NavigationContainer>
    </Provider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
