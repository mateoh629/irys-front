import React, { useEffect, useState } from 'react'
import { View , Text, StyleSheet, Dimensions, Button } from 'react-native'
import MapView, { Marker } from 'react-native-maps'
import * as Permissions from 'expo-permissions'
import * as Location from 'expo-location'
import { useTranslation } from 'react-i18next'

export function Map( { navigation } ) {

  const [locationPermission, setLocationPermission] = useState('denied')
  const [location, setLocation] = useState({
    latitude: 58,
    longitude: -22
  })
  const { t } = useTranslation()

  useEffect(() => {
    Permissions.askAsync(Permissions.LOCATION)
      .then(({ status }) => setLocationPermission(status))
  }, [])
  
  async function handleLocation() {
    const data = await Location.getCurrentPositionAsync({
      accuracy: Location.Accuracy.High
    })
    setLocation({
      latitude: data.coords.latitude,
      longitude: data.coords.longitude
    })
    alert(`${t('map.alert')} lat: ${location.latitude} lng: ${location.longitude}`)
  }

  return (
    <View style={styles.container}>
      <MapView 
        style={styles.map}
        showsMyLocationButton={true}
        zoomEnabled={true}
        zoomControlEnabled={true}
        zoomTapEnabled={true}
        showsUserLocation={true}
        showsScale={true}
        mapType='standard'
        provider='google'
      >
        <Marker
          coordinate={{latitude: location.latitude, longitude: location.longitude}}
          pinColor={'red'}
          title={'title'}
        />
      </MapView>
      <View style={styles.button}>
        <Button
          title={t('map.button')}
          onPress={handleLocation}
          color='#FFFFFF'
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    map: {
      flex: 1,
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height
    },
    button: {
      position: 'absolute',
      bottom: 20,
      borderRadius: 10,
      marginLeft: 10,
      backgroundColor: '#fd5c63',
    }
  })