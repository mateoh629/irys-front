export const icon = name => {
    switch (name) {
      case 'Lodging':
        return 'bed-outline'
      case 'Construction tools':
        return 'construct-outline'
      case 'Selling or leasing fuel':
        return 'speedometer-outline'
      case 'Other':
        return 'swap-horizontal-outline'
      case 'Medicine':
        return 'eyedrop-outline'
      case 'Building materials':
        return 'build-outline'
      case 'Food':
        return 'fast-food-outline'
    }
  }