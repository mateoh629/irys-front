const CHANGE = 'CHANGE'

export function changeLang(data) {
  return {
    type: CHANGE,
    payload: data
  }
}

const initialState = {
  lang: ''
}

export function languageReducer(prevState = initialState, action) {
  switch(action.type) {
    case CHANGE:
      return {
        ...prevState,
        lang: action.payload
      }
    default:
      return prevState
  }
}