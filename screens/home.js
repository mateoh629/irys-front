import React, { useCallback } from 'react'
import axios from 'axios'
import { useDispatch } from 'react-redux'
import { useFocusEffect } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { StyleSheet } from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import { useTranslation } from 'react-i18next'
import { receive, nodata } from '../store/categoriesReducer'
import { DrawerC } from '../components/drawer'
import { SubCategories } from '../components/subcategories'
import { AddDescription } from '../components/addDescription'
import { Categories } from '../components/categories'

const Stack = createStackNavigator()

export function Home( { navigation , ...props } ) {

  const dispatch = useDispatch()
  const { t } = useTranslation()

  useFocusEffect(
    useCallback( () => {
      axios({
        method : 'GET',
        url : 'http://localhost:8000/'
      })
      .then( (data) => {
        let categories = []
        for (let key in data.data) {
          categories.push(data.data[key])
        }
        dispatch(receive(categories))
      })
      .catch( function ( err ) {
        console.log( err )
        dispatch(nodata())
      })
    }, [] )
  )

  return (
    <Stack.Navigator>
      <Stack.Screen name='categories' options={{ title: t('header.title') }}>
        {props => <DrawerC component={Categories} name='categories' {...props}/>}
      </Stack.Screen>
      <Stack.Screen name='subcategories' options={{ title: t('header.title') }}>
        {props => <DrawerC component={SubCategories} name='subcategories' {...props}/>}
      </Stack.Screen>
      <Stack.Screen name='addDescription' options={{ title: t('header.title') }}>
        {props => <DrawerC component={AddDescription} name='addDescription' {...props}/>}
      </Stack.Screen>
    </Stack.Navigator>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})