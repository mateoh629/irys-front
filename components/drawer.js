import React , { useState } from 'react'
import { View, Button, StyleSheet } from 'react-native'
import { createDrawerNavigator, DrawerContentScrollView } from '@react-navigation/drawer'
import {
  Drawer,
  TouchableRipple
} from 'react-native-paper'
import { useTranslation } from 'react-i18next'
import { useSelector, useDispatch } from 'react-redux'
import { changeLang } from '../store/languageReducer'

function DrawerContent({navigation, ...props}) {

  const { t, i18n } = useTranslation()
  const dispatch = useDispatch()
  const language = useSelector(state => state.languageReducer.lang)
  const [toggle, setToggle] = useState(true)

  const onChangeLanguage = () => {
    i18n.changeLanguage(language)
    if (language === 'es') {
      dispatch(changeLang('en'))
      setToggle(true)
    }
    else {
      dispatch(changeLang('es'))
      setToggle(false)
    }
  }

  return (
    <DrawerContentScrollView>
      <View>
        <Drawer.Section>
          <TouchableRipple onPress={onChangeLanguage} style={styles.containter}>
            <View pointerEvents='none' style={styles.button}>
              <Button color='#FFFFFF' title={t('drawer.caption')}/>
            </View>
          </TouchableRipple>
        </Drawer.Section>
      </View>
    </DrawerContentScrollView>
  )
}

export function DrawerC({navigation, component, name, ...props}) {
  
  const Drawer = createDrawerNavigator()

  return (
    <Drawer.Navigator drawerContent={() => <DrawerContent/>}>
      <Drawer.Screen name={`${name}`} component={component}/>
    </Drawer.Navigator>
  )
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 10,
    marginLeft: 10,
    backgroundColor: '#fd5c63',
    width: '70%',
    marginLeft: 40
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})