import React, { useState } from 'react'
import { View , Text, StyleSheet, TextInput } from 'react-native'
import { useTranslation } from 'react-i18next'

export function AddDescription( { navigation , ...props } ) {

  const [text, setText] = useState('')
  const{ t } = useTranslation()

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{t('description.title')}</Text>
      <Text style={styles.subTitle}>{t('description.subTitle')}</Text>
      <TextInput
        multiline={true}
        numberOfLines={4}
        placeholder={t('description.placeholder')}
        value={text}
        onChangeText={setText}
        style={styles.input}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffe8e8',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 50
  },
  input: {
    borderColor: '#000000',
    borderWidth: 1,
    borderRadius: 5,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    padding: 10,
    height: 100,
    width: '95%'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 25,
    margin: 10,
    fontFamily: 'Helvetica'
  },
  subTitle: {
    margin: 10,
    fontSize: 20,
    fontFamily: 'Helvetica'
  }
})