import { createStore, combineReducers } from 'redux'
import { categoriesReducer } from './categoriesReducer'
import { languageReducer } from './languageReducer'

const rootReducer = combineReducers({ categoriesReducer, languageReducer })

export const store = createStore(rootReducer)